public class Tests {

    public Tests() {
    }


    @BeforeSuite()
    public void beforeTest() {
        System.out.println("BeforeTest");
    }

    @Test(priority = 4)
    public void fourthTest() {
        System.out.println("FourthTest");
    }
    @Test(priority = 1)
    public void firstTest(){
        System.out.println("FirstTest");
    }
    @Test(priority = 1)
    public void NofirstTest(){
        System.out.println("FirstTest1");
    }

    @Test(priority = 2)
    public void secondTest(){
        System.out.println("SecondTest");
    }
    @Test(priority = 3)
    public void thirdTest(){
        System.out.println("ThirdTest");
    }

    @AfterSuite()
    public void afterTest() {
        System.out.println("AfterTest");
    }
}
