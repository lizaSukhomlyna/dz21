import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TestRunner {
    public static void start(Class cl){
        try {
            Object obj = cl.getConstructor().newInstance(null);
            List<Method> prepareMethods = Arrays.stream(cl.getDeclaredMethods())
                    .filter(method -> method.isAnnotationPresent(BeforeSuite.class))
                    .collect(Collectors.toList());
            if (prepareMethods.size() != 1) throw new RuntimeException("Annotation 'BeforeSuite' can be used once");
            else {
                prepareMethods.get(0).invoke(obj);
            }

            List<Method> testMethods = Arrays.stream(cl.getDeclaredMethods())
                    .filter(method -> method.isAnnotationPresent(Test.class))
                    .sorted(Comparator.comparingInt(o -> o.getAnnotation(Test.class).priority()))
                    .collect(Collectors.toList());
            for (Method method : testMethods) {
                method.invoke(obj);
            }

            List<Method> afterMethods = Arrays.stream(cl.getDeclaredMethods())
                    .filter(method -> method.isAnnotationPresent(AfterSuite.class))
                    .collect(Collectors.toList());
            if (afterMethods.size() != 1) throw new RuntimeException("Annotation 'AfterSuite' can be used once");
            else {
                afterMethods.get(0).invoke(obj);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
